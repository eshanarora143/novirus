// this is a node js file when executed it will shutdown the machine
const exec = require('child_process').exec;

function shutdown(){
    exec('shutdown /l',(err, stdout, stderr) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(stdout);
});
}
shutdown();
